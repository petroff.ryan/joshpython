Python csv adventure

July 12

22:00 start - one hour limit, $30 hour

Josh has to find the data to give to Ryan

Ryan has suggested we use a git repo for shared work - https://gitlab.com/petroff.ryan/joshpython is set up and ready to go

Ryan has updated fresh linux install with latest python 3.8

Josh is getting data for Ryan to share

22:24 - call back to Josh

Given an ontario zip code, retrieve an area code,

Given an area code, retrieve the data from a database file (demographic data)

Given a collection of demographic data, display only a specific subset

And do all three at once:

Given a zip code, display demographic data subset

Right now there's 2 files: a zip-to-area-code mapping, and an area demographic data file. Both are CSV.

Csv is 4.7 gigs large, so free transfer service https://transfer.pcloud.com/ is being used

Learning is as important as execution for this project.

For outcomes, one intermediary output will be a reduced size CSV for easier work and transfer.

Instructions for the (re)creation of the subset will be a deliverable. As will heavily commented literate code.

Reconvene Tuesday 2-4pm

July 13

Added truncated.csv and test.py, off the clock

July 14

Updated test.py, off the clock

Payment confirmed, all hours now at reduced rate 65/h

14:00 phonecall to Josh

We're on a roll!
Got the second datafile in a merged pull request, solid.

Getting Josh the git clone :)

14:15 - switching computers to be able to use Zoom+record
