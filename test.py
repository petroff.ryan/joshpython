import pandas

chunk_container = pandas.read_csv('./98-401-X2016044_ONTARIO_English_CSV_data.csv', sep=',', chunksize=100)

for chunk in chunk_container:
    print("The Geo Code is %s" % chunk["DIM: Profile of Dissemination Areas (2247)"])

### with help from https://www.csvexplorer.com/blog/open-big-csv/
